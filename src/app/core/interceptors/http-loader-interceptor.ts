import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import { PageLoaderService } from '../services/page-loader.service';

@Injectable()
export class HttpLoaderInterceptor implements HttpInterceptor {

  private totalRequests = 0;

  constructor(private loaderService: PageLoaderService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    this.loaderService.setLoading(true);
    this.totalRequests++;

    return next.handle(request).pipe(
        tap(event => {
            return event;
        }),
        finalize( () => {
            this.totalRequests--;
            if (this.totalRequests === 0) {
                this.loaderService.setLoading(false);
            }
        })
    );
  }
}
