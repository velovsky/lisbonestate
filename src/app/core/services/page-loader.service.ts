import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PageLoaderService {

  // config
  private delay = 0.25; // in seconds

  // Observable sources
  private isLoading = new BehaviorSubject<boolean>(false);

  // Observable streams
  public isLoading$ = this.isLoading.asObservable();

  // Service message commands
  public setLoading(input: boolean) {
    if (input) {
      this.isLoading.next(input);
    } else {
      setTimeout(() => this.isLoading.next(input), this.delay * 1000);
    }
  }
}
