import { Component } from '@angular/core';
import { PageLoaderService } from '@app/core/services/page-loader.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent {

  constructor(private pageLoaderService: PageLoaderService) {
    pageLoaderService.setLoading(true);
  }

  // when video is loaded TODO: fire up a loader
  videoLoaded(event: Event) {
    this.pageLoaderService.setLoading(false);
  }

}
