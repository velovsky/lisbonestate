import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  public isExpanded: boolean;
  public isForced: boolean; // forced white bg color
  private forcedPages = ['/areas', '/contact'];

  constructor(private router: Router) {
    router.events.subscribe( (event) => {
      if (event instanceof NavigationEnd) {
        this.computeIsForced();
      }
    });
  }

  ngOnInit() {
    this.isExpanded = false; // init
    this.computeIsForced();
    window.addEventListener('scroll', this.onScrollEvent);
  }

  // exceptions for white bg color
  computeIsForced(): void {
    this.isForced = this.forcedPages.indexOf(this.router.url) >= 0 ? true : false;
  }

  // set transition animation: background width & color
  onScrollEvent = (): void => {
    this.isExpanded = window.scrollY > 0 ? true : false;
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.onScrollEvent);
  }

}
