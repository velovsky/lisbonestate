import { Component, OnInit, OnDestroy } from '@angular/core';
import { PageLoaderService } from '@app/core/services/page-loader.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-page-loader',
  templateUrl: './page-loader.component.html',
  styleUrls: ['./page-loader.component.scss']
})
export class PageLoaderComponent implements OnInit, OnDestroy {

  public enterLoader: boolean;
  public divArray = Array(9).fill(undefined); // define # div's for loader fx
  private loaderSubscription: Subscription;

  constructor(pageLoaderService: PageLoaderService) {
    this.loaderSubscription = pageLoaderService.isLoading$.subscribe(isLoading => {
      this.enterLoader = isLoading;
      window.document.querySelector('body').style.overflow = isLoading ? 'hidden' : 'auto';
    });
  }

  ngOnInit() {
  }

  public progBarFxClass() {
    return {
      fadeIn: this.enterLoader,
      fadeOut: !this.enterLoader
    };
  }

  public bgFxClass() {
    return {
      fadeOut: !this.enterLoader
    };
  }

  ngOnDestroy() {
    this.loaderSubscription.unsubscribe();
  }
}
