import { Component } from '@angular/core';
import { OnLoadComponent } from '@app/shared/on-load-component/on-load-component';
import { ImageConfigService } from '@app/shared/services/image-config.service';
import { PageLoaderService } from '@app/core/services/page-loader.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent extends OnLoadComponent {

  public homepageBackgroundURL = '/assets/homepage1.jpg';

  constructor(
    imageConfig: ImageConfigService,
    public pageLoaderService: PageLoaderService
    ) {
    super(pageLoaderService);
    const optimizedBG: string = imageConfig.getOptimizedUrl(this.homepageBackgroundURL);
    this.homepageBackgroundURL = `url('${optimizedBG}')`;
  }

}
