import { TestBed } from '@angular/core/testing';

import { ImageConfigService } from './image-config.service';

describe('ImageConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImageConfigService = TestBed.get(ImageConfigService);
    expect(service).toBeTruthy();
  });
});
