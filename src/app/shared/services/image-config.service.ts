import { Injectable } from '@angular/core';
import { UserAgentService, BrowsersEnum } from './user-agent.service';

@Injectable({
  providedIn: 'root'
})
export class ImageConfigService {

  private browser: BrowsersEnum;

  constructor(userAgent: UserAgentService) {
    this.browser = userAgent.getBrowser();
  }

  /*
    WARNING:
    needs to 1st run "convert.bat" to create *.webp images
    or this implicit operation will fail
  */
  // converts to "webp" for Chrome/Firefox browsers
  // convert to "jp2" for Safari browsers
  // else uses default img src
  public getOptimizedUrl(url: string): string {
    let output: string;
    switch (this.browser) {
      case BrowsersEnum.CHROME:
      case BrowsersEnum.FIREFOX:
        output = this.changeFormat(url, '.webp')
        break;
      case BrowsersEnum.SAFARI:
        output = this.changeFormat(url, '.jp2')
        break;
      default:
        output = url;
    }
    return output;
  }

  private changeFormat(url: string, extension: string): string {
    const temp: string[] = url.split('.');
    const output: string = temp[0] + extension;
    return output;
  }
}
