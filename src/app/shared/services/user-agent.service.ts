import { Injectable } from '@angular/core';

export enum BrowsersEnum {
  FIREFOX = 'Firefox',
  CHROME = 'Chrome',
  SAFARI = 'Safari',
  OTHERS = 'Others'
}

class BrowsersRegex {
  public static readonly FIREFOX = /^(?!.*(Seamonkey)).*(Firefox).*/;
  public static readonly CHROME = /^(?!.*(Chromium)).*(Chrome).*/;
  public static readonly SAFARI = /^(?!.*(Chrome|Chromium)).*(Safari).*/;
}

@Injectable({
  providedIn: 'root'
})
export class UserAgentService {

  constructor() { }

  public getBrowser(): BrowsersEnum {
    const UA = navigator.userAgent;

    if (BrowsersRegex.FIREFOX.test(UA)) {
      return BrowsersEnum.FIREFOX;
    } else if (BrowsersRegex.CHROME.test(UA)) {
      return BrowsersEnum.CHROME;
    } else if (BrowsersRegex.SAFARI.test(UA)) {
      return BrowsersEnum.SAFARI;
    } else {
      return BrowsersEnum.OTHERS;
    }
  }
}
