import { Component, AfterViewInit, QueryList, ViewChildren } from '@angular/core';
import { PageLoaderService } from '@app/core/services/page-loader.service';
import { ImgWrapperComponent } from '../img-wrapper/img-wrapper.component';

@Component({
  selector: 'app-on-load-component',
  template: ''
})
export class OnLoadComponent implements AfterViewInit {

  private numImgs = 0;
  @ViewChildren(ImgWrapperComponent) imageArray: QueryList<ImgWrapperComponent>;

  constructor(public pageLoaderService: PageLoaderService) {
  }

  ngAfterViewInit() {
    if (this.imageArray.length > 0) {
      this.pageLoaderService.setLoading(true);
    }
    this.imageArray.forEach( (element) => {
      this.numImgs++;
      element.callback = this.onLoadedImage.bind(this);
    });
  }

  onLoadedImage(): void {
    this.numImgs--;
    if (this.numImgs === 0) {
      this.pageLoaderService.setLoading(false);
    }
  }

}
