import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImgWrapperComponent } from './img-wrapper/img-wrapper.component';
import { OnLoadComponent } from './on-load-component/on-load-component';

@NgModule({
  declarations: [ImgWrapperComponent, OnLoadComponent],
  imports: [
    CommonModule
  ],
  exports: [ImgWrapperComponent]
})
export class SharedModule { }
