import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ImageConfigService } from '../services/image-config.service';

@Component({
  selector: 'app-img-wrapper',
  templateUrl: './img-wrapper.component.html',
  styleUrls: ['./img-wrapper.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ImgWrapperComponent {

  constructor(private imageConfig: ImageConfigService) {
  }

  // tslint:disable-next-line: variable-name
  private _url = '';

  get url(): string {
    return this._url;
  }

  @Input('src')
  set url(url: string) {
    this._url = this.imageConfig.getOptimizedUrl(url);
  }
  @Input('load') callback: () => void;

}
