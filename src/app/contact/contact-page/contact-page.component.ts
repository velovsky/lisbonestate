import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, AbstractControl } from '@angular/forms';
import * as emailjs from 'emailjs-com';
import { EmailJSResponseStatus } from 'emailjs-com/source/models/EmailJSResponseStatus';
import { Observable, from } from 'rxjs';
import { AreaModel } from '@app/areas/models/area-model';
import { AreaMapperService } from '@app/areas/services/area-mapper.service';
import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {

  public isBtnDisabled: boolean;
  public areas: AreaModel[];
  public budgets: string[];
  public myForm: FormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    area: [[]],
    budget: [''],
    message: ['', Validators.required]
  });

  constructor(
    private formBuilder: FormBuilder,
    private areaMapper: AreaMapperService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.isBtnDisabled = false;
    emailjs.init(environment.emailjsKey);
    this.areas = this.areaMapper.generateAreas();
    this.areas.push({ name: 'OTHER', code: 'OTHER' }); // other option
    this.budgets = ['< 200k', '200k - 500k', '> 500k'];
  }

  public getControler(controlerName: string): AbstractControl {
    return this.myForm.get(controlerName);
  }

  public submit(): void {

    const inputs = {
      from_name: this.getControler('email').value,
      reply_to: this.getControler('email').value,
      areas: this.getControler('area').value.toString(), // array
      budget: this.getControler('budget').value,
      message_html: this.getControler('message').value
    };

    this.isBtnDisabled = true;

    const promise: Promise<EmailJSResponseStatus> = emailjs.send('realestate', 'template_IQYG9EQD_clone', inputs);
    const observable: Observable<EmailJSResponseStatus> = from(promise);

    observable.subscribe( response => {
      this.isBtnDisabled = false;
      this.snackBar.open('Message submitted successfully', 'Close', {duration: 5000, verticalPosition: 'top'});
    }, (error) => {
      this.isBtnDisabled = false;
      this.snackBar.open('There was an error, please try again later', 'Close', {duration: 5000, verticalPosition: 'top'});
    });
  }


}
