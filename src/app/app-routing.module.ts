import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './core/components/landing-page/landing-page.component';
import { environment } from 'src/environments/environment';

const routes: Routes = [
  {
    path: '',
    component: LandingPageComponent
  },
  {
    path: '', children: [
        {
          path: 'home',
          loadChildren: '@app/homepage/homepage.module#HomepageModule'
        },
        {
          path: 'areas',
          loadChildren: '@app/areas/areas.module#AreasModule'
        },
        {
          path: 'contact',
          loadChildren: '@app/contact/contact.module#ContactModule'
        }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
