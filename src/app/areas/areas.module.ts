import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AreasRoutingModule } from './areas-routing.module';
import { AreasComponent } from './areas.component';
import { AreaWrapperComponent } from './area-wrapper/area-wrapper.component';
import { AreaInfoPageComponent } from './area-info-page/area-info-page.component';
import { SharedModule } from '@app/shared/shared.module';

@NgModule({
  declarations: [AreasComponent, AreaWrapperComponent, AreaInfoPageComponent],
  imports: [
    CommonModule,
    AreasRoutingModule,
    SharedModule
  ]
})
export class AreasModule { }
