import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AreasComponent } from './areas.component';
import { AreaInfoPageComponent } from './area-info-page/area-info-page.component';

const routes: Routes = [
  {
    path: '', component: AreasComponent
  },
  {
    path: ':id', component: AreaInfoPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AreasRoutingModule { }
