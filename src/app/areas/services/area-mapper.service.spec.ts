import { TestBed } from '@angular/core/testing';

import { AreaMapperService } from './area-mapper.service';

describe('AreaMapperService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AreaMapperService = TestBed.get(AreaMapperService);
    expect(service).toBeTruthy();
  });
});
