import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AreaInfoModel } from '../models/area-info-model';
import { AreaConfigService } from './area-config.service';

@Injectable({
  providedIn: 'root'
})
export class FetchDataService {

  constructor(
    private http: HttpClient,
    private areaConfig: AreaConfigService
    ) { }

  public getAreaInfo(areaCode: string): Observable<AreaInfoModel> {
    return this.http.get<AreaInfoModel>(this.areaConfig.getInfo(areaCode));
  }
}
