import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AreaConfigService {

  private areasPath = '/assets/areas';
  private iconName = 'icon.jpg';
  private infoName = 'info.json';
  private imgFormat = 'jpg';
  private totalNumPhotos = 5;

  private getAreaPath(areaCode: string): string {
    return `${this.areasPath}/${areaCode}`;
  }

  public getIcon(areaCode: string): string {
    return `${this.getAreaPath(areaCode)}/${this.iconName}`;
  }

  public getInfo(areaCode: string): string {
    return `${this.getAreaPath(areaCode)}/${this.infoName}`;
  }

  public getGallery(areaCode: string): string[] {
    const output: string[] = [];

    for (let i = 1; i <= this.totalNumPhotos; i++) {
      output.push(`${this.getAreaPath(areaCode)}/${i}.${this.imgFormat}`);
    }

    return output;
  }

  public getTotalNumberOfPhotos(): number {
    return this.totalNumPhotos;
  }
}
