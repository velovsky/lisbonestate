import { Injectable } from '@angular/core';
import { AreaModel } from '../models/area-model';
import { Freguesias } from '../models/freguesias.enum';

@Injectable({
  providedIn: 'root'
})
export class AreaMapperService {

  public generateAreas(): AreaModel[] {
    const areaCodes: string[] = Object.keys(Freguesias).map( freguesia => freguesia );
    const areas: AreaModel[] = areaCodes.map( fregKey => this.map(fregKey) );

    return areas;
  }

  public map(areaCode: string): AreaModel {
    const areaName: string = Freguesias[areaCode];

    const output: AreaModel = {
      name: areaName,
      code: areaCode
    };

    return output;
  }

}
