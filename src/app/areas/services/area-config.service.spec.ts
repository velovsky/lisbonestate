import { TestBed } from '@angular/core/testing';

import { AreaConfigService } from './area-config.service';

describe('AreaConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AreaConfigService = TestBed.get(AreaConfigService);
    expect(service).toBeTruthy();
  });
});
