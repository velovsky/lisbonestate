export interface AreaModel {
  name: string;
  code: string;
}
