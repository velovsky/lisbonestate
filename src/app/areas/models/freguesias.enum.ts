export enum Freguesias {
  avenida_da_liberdade = 'AVENIDA DA LIBERDADE',
  alcantara = 'ALCÂNTARA',
  belem = 'BELÉM',
  estrela = 'ESTRELA',
  parque_das_nacoes = 'PARQUE DAS NAÇÕES'
}
