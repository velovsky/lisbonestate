import { Component, OnInit, Input } from '@angular/core';
import { AreaModel } from '../models/area-model';
import { AreaConfigService } from '../services/area-config.service';
import { ImageConfigService } from '@app/shared/services/image-config.service';

@Component({
  selector: 'app-area-wrapper',
  templateUrl: './area-wrapper.component.html',
  styleUrls: ['./area-wrapper.component.scss']
})
export class AreaWrapperComponent implements OnInit {

  @Input()
  set area(area: AreaModel) {
    const { name, code } = area;
    this.areaName = name;
    this.areaCode = code;
  }

  public areaName: string;
  public areaCode: string;
  public imageBackgroundURL: string;

  constructor(
    private areaConfig: AreaConfigService,
    private imageConfig: ImageConfigService
    ) { }

  ngOnInit() {
    const icon: string = this.areaConfig.getIcon(this.areaCode);
    const imgURL: string = this.imageConfig.getOptimizedUrl(icon);
    this.imageBackgroundURL = `url('${imgURL}')`;
  }

}
