import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaWrapperComponent } from './area-wrapper.component';

describe('AreaWrapperComponent', () => {
  let component: AreaWrapperComponent;
  let fixture: ComponentFixture<AreaWrapperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaWrapperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
