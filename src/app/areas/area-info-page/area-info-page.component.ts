import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AreaModel } from '../models/area-model';
import { AreaMapperService } from '../services/area-mapper.service';
import { FetchDataService } from '../services/fetch-data.service';
import { AreaConfigService } from '../services/area-config.service';
import { OnLoadComponent } from '@app/shared/on-load-component/on-load-component';
import { PageLoaderService } from '@app/core/services/page-loader.service';

@Component({
  selector: 'app-area-info-page',
  templateUrl: './area-info-page.component.html',
  styleUrls: ['./area-info-page.component.scss']
})
export class AreaInfoPageComponent extends OnLoadComponent implements OnInit {

  @ViewChild('gallery') galleryElement: ElementRef;

  // page data
  public area: AreaModel;
  public texto: string;

  // gallery
  private galleryIndex: number;
  private TOTAL_IMAGES: number;
  public galleryImages: string[];
  public disableNext = false;
  public disablePrevious = true;

  constructor(
    private route: ActivatedRoute,
    private areaMapper: AreaMapperService,
    private fetchData: FetchDataService,
    private areaConfig: AreaConfigService,
    public pageLoaderService: PageLoaderService
    ) {
      super(pageLoaderService);
    }

  ngOnInit() {
    const areaCode = this.route.snapshot.paramMap.get('id');

    this.TOTAL_IMAGES = this.areaConfig.getTotalNumberOfPhotos();

    // fetch area data
    this.area = this.areaMapper.map(areaCode); // retrieve area object
    this.fetchData.getAreaInfo(areaCode).subscribe(res => this.texto = res.info);
    this.galleryImages = this.areaConfig.getGallery(areaCode);

    this.galleryIndex = 0;
  }

  // go through gallery
  switchPicture(increment: boolean) {
    if (increment && (this.galleryIndex + 1 < this.TOTAL_IMAGES)) {
      this.galleryIndex += 1;
    } else if (!increment && (this.galleryIndex - 1 >= 0)) {
      this.galleryIndex -= 1;
    }

    // move images through gallery
    this.galleryElement.nativeElement.style.right = (this.galleryIndex * 100).toString() + '%';

    // update navigation buttons state
    this.disablePrevious = this.galleryIndex === 0;
    this.disableNext = this.galleryIndex === this.TOTAL_IMAGES - 1;
  }

}
