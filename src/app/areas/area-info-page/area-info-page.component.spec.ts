import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaInfoPageComponent } from './area-info-page.component';

describe('AreaInfoPageComponent', () => {
  let component: AreaInfoPageComponent;
  let fixture: ComponentFixture<AreaInfoPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaInfoPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaInfoPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
