import { Component, OnInit } from '@angular/core';
import { AreaModel } from './models/area-model';
import { AreaMapperService } from './services/area-mapper.service';
import { OnLoadComponent } from '@app/shared/on-load-component/on-load-component';
import { PageLoaderService } from '@app/core/services/page-loader.service';
import { AreaConfigService } from './services/area-config.service';
import { ImageConfigService } from '@app/shared/services/image-config.service';

@Component({
  selector: 'app-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss']
})
export class AreasComponent extends OnLoadComponent implements OnInit {

  public areas: AreaModel[];
  public imgURLs: string[];

  constructor(
    private areaMapper: AreaMapperService,
    private areaConfig: AreaConfigService,
    private imageConfig: ImageConfigService,
    public pageLoaderService: PageLoaderService
  ) {
    super(pageLoaderService);
  }

  ngOnInit() {
    this.areas = this.areaMapper.generateAreas();
    // workaround for img load events
    this.imgURLs = this.areas.map( area => {
      const areaIcon = this.areaConfig.getIcon(area.code);
      const imgURL = this.imageConfig.getOptimizedUrl(areaIcon);
      return imgURL;
    });
  }

}
