import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  constructor(private router: Router) {
    // Between routings, always scroll to top
    router.events.subscribe( (event) => {
      if (event instanceof NavigationStart) {
        window.scrollTo(0, 0);
      }
    });
  }

  ngOnInit() {
  }

  public showHeader(): boolean {
    return this.router.url !== '/';
  }

  // public togglePageFx(): void {

  //   const layerClass = '.top-layer';
  //   const layers: NodeListOf<Element> = document.querySelectorAll(layerClass);
  //   layers.forEach( (element) => element.classList.toggle('active') );
  // }

}
