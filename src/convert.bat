@echo off

cls
REM TO CREATE WEBP
setlocal enabledelayedexpansion
REM 1st argument: extension to convert
set extension=%1
REM 2nd argument: quality of picture
set quality=%2
REM remove old files first (if exist)
call remove.bat %extension%
REM generate files
for /r %%f in ("*") do (
	set filepath=%%~pf
	if "!filepath!"=="!filepath:icons=!" (		
		set true=0
		if "%%~xf"==".jpg" set true=1
		if "%%~xf"==".png" set true=1
		if "!true!"=="1" (
			magick convert %%f -quality %quality% %%~pf%%~nf.%extension%
			echo %%f ...done
		)
	)
)
endlocal