@echo off

cls
REM TO REMOVE WEBP
REM 1st argument: extension to delete
set extension=%1
for /r %%f in ("*") do (
	if "%%~xf" == ".%extension%" (
		del %%~pf%%~nf.%extension%
		echo %%f ...done
	)
)